def pca(data, reduced_dim):

# implementation of principal components analysis
# PCA finds components that maiximize variance in data
# data should have components to be reduced in 1st dim

import numpy as np

# PCA 
orig_dim = data.shape[0]
meanVec = data.mean(axis=1)

# zero-mean covariance matrix
for i in range(data.shape[1]):
    data_zeromean = data - data.mean(axis=1,keepdims=True)
    covMatrix = (data_zeromean).dot((data_zeromean).T)

# computing eigenvectors and eigenvalues
eig, evec = np.linalg.eig(covMatrix) # evec[:,i] is vector

# sort eigenvectors by decreasing eigenvalues
eig_pairs = [(np.abs(eig[i]), evec[:,i]) for i in range(len(eig))]

# sort (eig, ev) tuple from hi to low
eig_pairs.sort(key=lambda x: x[0], reverse = True)

# choose most informative k eigenvectors
reduced_mat = eig_pairs[0][1].reshape(3,1)
for pair in eig_pairs:
    reduced_mat = np.hstack((reduced_mat, pair[1].reshape(3,1)))
reduced_mat = reduced_mat[:,1:] # remove duplicate 1st column

# transform samples onto subspace y = W.T * x; mean-centred
reduced_input = reduced_mat.T.dot(data-mean_vector)

return reduced_input


